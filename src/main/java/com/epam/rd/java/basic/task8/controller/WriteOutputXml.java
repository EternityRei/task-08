package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.models.Constants;
import com.epam.rd.java.basic.task8.models.Flower;
import com.epam.rd.java.basic.task8.models.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.List;

import static javax.xml.XMLConstants.W3C_XML_SCHEMA_NS_URI;

public class WriteOutputXml {

    public boolean validateXML(String xsdFileName, String xmlFileName) {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(W3C_XML_SCHEMA_NS_URI);
        try {
            ((schemaFactory.newSchema(new File(xsdFileName)))
                    .newValidator()).validate(new StreamSource(new File(xmlFileName)));
            return true;
        } catch (SAXException | IOException e) {
            System.out.println(e.getMessage());
            return false;
        }
    }

    public void createOutputXml(Flowers flowers, String outputFile) throws ParserConfigurationException {
        DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
        List<Flower> flowerList = flowers.getFlowerList();

        Document doc = docBuilder.newDocument();
        Element rootElement = doc.createElement(flowers.getRoot());
        rootElement.setAttribute(Constants.xmlns[0], Constants.xmlns[1]);
        rootElement.setAttribute(Constants.schemaXmlns[0], Constants.schemaXmlns[1]);
        rootElement.setAttribute(Constants.schemaLocation[0], Constants.schemaLocation[1]);
        doc.appendChild(rootElement);

        for (Flower value : flowerList) {
            Element flower = doc.createElement(value.getRoot());
            rootElement.appendChild(flower);

            Element name = doc.createElement(Constants.NAME);
            name.setTextContent(value.getName());
            flower.appendChild(name);

            Element soil = doc.createElement(Constants.SOIL);
            soil.setTextContent(value.getSoil());
            flower.appendChild(soil);

            Element origin = doc.createElement(Constants.ORIGIN);
            origin.setTextContent(value.getOrigin());
            flower.appendChild(origin);

            Flower.VisualParameters visualParameters = value.getVisualParameters();

            Element visualParameter = doc.createElement(Constants.VISUAL_PARAMETERS);
            flower.appendChild(visualParameter);

            Element stemColour = doc.createElement(Constants.STEM_COLOUR);
            stemColour.setTextContent(visualParameters.getStemColour());
            visualParameter.appendChild(stemColour);

            Element leafColour = doc.createElement(Constants.LEAF_COLOUR);
            leafColour.setTextContent(visualParameters.getLeafColour());
            visualParameter.appendChild(leafColour);

            Element aveLenFlower = doc.createElement(Constants.AVE_LEN_FLOWER);
            aveLenFlower.setTextContent(visualParameters.getAveLenFlower());
            aveLenFlower.setAttribute(Constants.MEASURE, visualParameters.getAveLenFlowerMeasure());
            visualParameter.appendChild(aveLenFlower);

            Flower.GrowingTips growingTips = value.getGrowingTips();

            Element growingTip = doc.createElement(Constants.GROWING_TIPS);
            flower.appendChild(growingTip);

            Element tempreture = doc.createElement(Constants.TEMPRETURE);
            tempreture.setTextContent(growingTips.getTempreture());
            tempreture.setAttribute(Constants.MEASURE, growingTips.getTempretureMeasure());
            growingTip.appendChild(tempreture);

            Element lighting = doc.createElement(Constants.LIGHTING);
            lighting.setAttribute(Constants.LIGHT_REQUIRING, growingTips.getLightingLightRequiring());
            growingTip.appendChild(lighting);

            Element watering = doc.createElement(Constants.WATERING);
            watering.setTextContent(growingTips.getWatering());
            watering.setAttribute(Constants.MEASURE, growingTips.getWateringMeasure());
            growingTip.appendChild(watering);

            Element multiplying = doc.createElement(Constants.MULTIPLYING);
            multiplying.setTextContent(value.getMultiplying());
            flower.appendChild(multiplying);
        }

        try (FileOutputStream output =
                     new FileOutputStream(outputFile)) {
            writeXml(doc, output);
        } catch (IOException | TransformerException e) {
            e.printStackTrace();
        }
    }

    public void writeXml(Document doc, OutputStream output) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        transformer.setOutputProperty(OutputKeys.INDENT, "yes");

        DOMSource source = new DOMSource(doc);
        StreamResult result = new StreamResult(output);

        transformer.transform(source, result);
    }

}
