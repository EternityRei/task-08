package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.models.Constants;
import com.epam.rd.java.basic.task8.models.Flower;
import com.epam.rd.java.basic.task8.models.Flowers;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parse(){
		Flowers flowers = new Flowers();
		List<Flower> flowerList = new LinkedList<>();
		Document document;
		try {
			document = getDocument(xmlFileName);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}

		Node node = document.getFirstChild();
		flowers.setRoot(node.getNodeName());
		NodeList nodeList = node.getChildNodes();
		Flower.VisualParameters visualParametersList;
		Flower.GrowingTips visualGrowingTipsList;

		for (int i = 0; i < nodeList.getLength(); i++) {
			NodeList innerFlowerList = nodeList.item(i).getChildNodes();
			if (innerFlowerList.getLength() == 0)
				continue;
			Flower flower = new Flower();
			flower.setRoot(nodeList.item(i).getNodeName());
			for (int j = 0; j < innerFlowerList.getLength(); j++) {
				if (innerFlowerList.item(j).getNodeType() != Node.ELEMENT_NODE) {
					continue;
				}
				switch (innerFlowerList.item(j).getNodeName()) {
					case Constants.NAME:
						flower.setName(innerFlowerList.item(j).getTextContent());
						break;
					case Constants.SOIL:
						flower.setSoil(innerFlowerList.item(j).getTextContent());
						break;
					case Constants.ORIGIN:
						flower.setOrigin(innerFlowerList.item(j).getTextContent());
						break;
					case Constants.VISUAL_PARAMETERS:
						visualParametersList = getVisualParameters(
								innerFlowerList.item(j).getChildNodes(),
								innerFlowerList.item(j).getNodeName()
						);
						flower.setVisualParameters(visualParametersList);
						break;
					case Constants.GROWING_TIPS:
						visualGrowingTipsList = getGrowingTips(
								innerFlowerList.item(j).getChildNodes(),
								innerFlowerList.item(j).getNodeName()
						);
						flower.setGrowingTips(visualGrowingTipsList);
						break;
					case Constants.MULTIPLYING:
						flower.setMultiplying(innerFlowerList.item(j).getTextContent());
						break;
					default:
						break;
				}
			}
			flowerList.add(flower);
		}
		flowers.setFlowerList(flowerList);
		return flowers;
	}

	private Flower.GrowingTips getGrowingTips(NodeList list, String root) {
		Flower.GrowingTips growingTips = new Flower.GrowingTips();
		growingTips.setRoot(root);
		for (int i = 0; i < list.getLength(); i++) {
			if(list.item(i).getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}

			switch (list.item(i).getNodeName()){
				case Constants.TEMPRETURE:
					growingTips.setTempreture(list.item(i).getTextContent());
					String measure = list.item(i).getAttributes().getNamedItem(Constants.MEASURE).getNodeValue();
					growingTips.setTempretureMeasure(measure);
					break;
				case Constants.LIGHTING:
					String lightRequiring = list.item(i).getAttributes().getNamedItem(Constants.LIGHT_REQUIRING).getNodeValue();
					growingTips.setLightingLightRequiring(lightRequiring);
					break;
				case Constants.WATERING:
					growingTips.setWatering(list.item(i).getTextContent());
					String wMeasure = list.item(i).getAttributes().getNamedItem(Constants.MEASURE).getNodeValue();
					growingTips.setWateringMeasure(wMeasure);
					break;
				default:
					break;
			}
		}
		return growingTips;
	}

	private Flower.VisualParameters getVisualParameters(NodeList list, String root) {
		Flower.VisualParameters visualParameters = new Flower.VisualParameters();
		visualParameters.setRoot(root);
		for (int i = 0; i < list.getLength(); i++) {
			if(list.item(i).getNodeType() != Node.ELEMENT_NODE) {
				continue;
			}
			switch (list.item(i).getNodeName()){
				case Constants.STEM_COLOUR:
					visualParameters.setStemColour(list.item(i).getTextContent());
					break;
				case Constants.LEAF_COLOUR:
					visualParameters.setLeafColour(list.item(i).getTextContent());
					break;
				case Constants.AVE_LEN_FLOWER:
					visualParameters.setAveLenFlower(list.item(i).getTextContent());
					String measure = list.item(i).getAttributes().getNamedItem(Constants.MEASURE).getNodeValue();
					visualParameters.setAveLenFlowerMeasure(measure);
					break;
				default:
					break;
			}
		}
		return visualParameters;
	}

	private Document getDocument(String XMLfile) throws Exception {
		File file = new File(XMLfile);
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		return dbf.newDocumentBuilder().parse(file);
	}


}
