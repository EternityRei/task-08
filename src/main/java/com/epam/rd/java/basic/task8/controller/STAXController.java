package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.models.Constants;
import com.epam.rd.java.basic.task8.models.Flower;
import com.epam.rd.java.basic.task8.models.Flowers;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.EndElement;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.List;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private String xmlFileName;
	private Flowers flowers;
	private List<Flower> flowerList;
	private Flower flower;
	private Flower.VisualParameters visualParameters;
	private Flower.GrowingTips growingTips;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public Flowers parse() throws FileNotFoundException, XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
		flowerList = new LinkedList<>();
		Attribute attr;
		while (reader.hasNext()) {
			XMLEvent nextEvent = reader.nextEvent();
			if (nextEvent.isStartElement()) {
				StartElement startElement = nextEvent.asStartElement();
				switch (startElement.getName().getLocalPart()) {
					case Constants.FLOWERS:
						nextEvent = reader.nextEvent();
						flowers = new Flowers();
						flowers.setRoot(startElement.getName().getLocalPart());
						break;
					case Constants.FLOWER:
						nextEvent = reader.nextEvent();
						flower = new Flower();
						flower.setRoot(startElement.getName().getLocalPart());
						break;
					case Constants.NAME:
						nextEvent = reader.nextEvent();
						flower.setName(nextEvent.asCharacters().getData());
						break;
					case Constants.SOIL:
						nextEvent = reader.nextEvent();
						flower.setSoil(nextEvent.asCharacters().getData());
						break;
					case Constants.ORIGIN:
						nextEvent = reader.nextEvent();
						flower.setOrigin(nextEvent.asCharacters().getData());
						break;
					case Constants.VISUAL_PARAMETERS:
						visualParameters = new Flower.VisualParameters();
						visualParameters.setRoot(startElement.getName().getLocalPart());
						break;
					case Constants.STEM_COLOUR:
						nextEvent = reader.nextEvent();
						visualParameters.setStemColour(nextEvent.asCharacters().getData());
						break;
					case Constants.LEAF_COLOUR:
						nextEvent = reader.nextEvent();
						visualParameters.setLeafColour(nextEvent.asCharacters().getData());
						break;
					case Constants.AVE_LEN_FLOWER:
						attr = startElement.getAttributeByName(new QName(Constants.MEASURE));
						nextEvent = reader.nextEvent();
						visualParameters.setAveLenFlower(nextEvent.asCharacters().getData());
						visualParameters.setAveLenFlowerMeasure(attr.getValue());
						break;
					case Constants.GROWING_TIPS:
						growingTips = new Flower.GrowingTips();
						growingTips.setRoot(startElement.getName().getLocalPart());
						break;
					case Constants.TEMPRETURE:
						attr = startElement.getAttributeByName(new QName(Constants.MEASURE));
						nextEvent = reader.nextEvent();
						growingTips.setTempreture(nextEvent.asCharacters().getData());
						growingTips.setTempretureMeasure(attr.getValue());
						break;
					case Constants.LIGHTING:
						attr = startElement.getAttributeByName(new QName(Constants.LIGHT_REQUIRING));
						nextEvent = reader.nextEvent();
						growingTips.setLightingLightRequiring(attr.getValue());
						break;
					case Constants.WATERING:
						attr = startElement.getAttributeByName(new QName(Constants.MEASURE));
						nextEvent = reader.nextEvent();
						growingTips.setWatering(nextEvent.asCharacters().getData());
						growingTips.setWateringMeasure(attr.getValue());
						break;
					case Constants.MULTIPLYING:
						nextEvent = reader.nextEvent();
						flower.setMultiplying(nextEvent.asCharacters().getData());
						break;
					default:
						break;
				}
			}
			if (nextEvent.isEndElement()) {
				EndElement endElement = nextEvent.asEndElement();
				switch (endElement.getName().getLocalPart()){
					case Constants.FLOWER:
						flowerList.add(flower);
						break;
					case Constants.VISUAL_PARAMETERS:
						flower.setVisualParameters(visualParameters);
						break;
					case Constants.GROWING_TIPS:
						flower.setGrowingTips(growingTips);
						break;
					case Constants.FLOWERS:
						flowers.setFlowerList(flowerList);
						break;
					default:
						break;
				}
			}
		}
		return flowers;
	}

}