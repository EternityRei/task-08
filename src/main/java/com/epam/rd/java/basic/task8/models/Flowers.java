package com.epam.rd.java.basic.task8.models;

import java.util.List;

public class Flowers {

    private List<Flower> flowerList;

    private String root;

    public Flowers() {
    }

    public Flowers(List<Flower> flowerList, String root) {
        this.flowerList = flowerList;
        this.root = root;
    }

    public List<Flower> getFlowerList() {
        return flowerList;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public void setFlowerList(List<Flower> flowerList) {
        this.flowerList = flowerList;
    }

    @Override
    public String toString() {
        return getRoot()+"{" +
                "flowerList=" + flowerList +
                "}\n";
    }
}
