package com.epam.rd.java.basic.task8.models;

import java.util.Objects;

public class Flower {

    private String root;
    private String name;
    private String soil;
    private String origin;
    private VisualParameters visualParameters;
    private GrowingTips growingTips;

    public static class VisualParameters {
        private String root;
        private String stemColour;
        private String leafColour;
        private String aveLenFlower;
        private String aveLenFlowerMeasure;


        public String getStemColour() {
            return stemColour;
        }

        public void setStemColour(String stemColour) {
            this.stemColour = stemColour;
        }

        public String getLeafColour() {
            return leafColour;
        }

        public void setLeafColour(String leafColour) {
            this.leafColour = leafColour;
        }

        public String getAveLenFlower() {
            return aveLenFlower;
        }

        public void setAveLenFlower(String aveLenFlower) {
            this.aveLenFlower = aveLenFlower;
        }

        public String getAveLenFlowerMeasure() {
            return aveLenFlowerMeasure;
        }

        public void setAveLenFlowerMeasure(String aveLenFlowerMeasure) {
            this.aveLenFlowerMeasure = aveLenFlowerMeasure;
        }

        public String getRoot() {
            return root;
        }

        public void setRoot(String root) {
            this.root = root;
        }

        @Override
        public String toString() {
            return "\t"+getRoot()+"{\n" +
                    "\t\tstemColour=" + stemColour + "\n" +
                    "\t\tleafColour=" + leafColour + "\n" +
                    "\t\taveLenFlower=" + aveLenFlower + "\n" +
                    "\t\taveLenFlowerMeasure=" + aveLenFlowerMeasure + "\n" +
                    "\t}\n";
        }
    }
    public static class GrowingTips {
        private String root;
        private String tempreture;
        private String tempretureMeasure;
        private String lightingLightRequiring;
        private String watering;
        private String wateringMeasure;

        public String getTempreture() {
            return tempreture;
        }

        public void setTempreture(String tempreture) {
            this.tempreture = tempreture;
        }

        public String getTempretureMeasure() {
            return tempretureMeasure;
        }

        public void setTempretureMeasure(String tempretureMeasure) {
            this.tempretureMeasure = tempretureMeasure;
        }


        public String getLightingLightRequiring() {
            return lightingLightRequiring;
        }

        public void setLightingLightRequiring(String lightingLightRequiring) {
            this.lightingLightRequiring = lightingLightRequiring;
        }

        public String getWatering() {
            return watering;
        }

        public void setWatering(String watering) {
            this.watering = watering;
        }

        public String getWateringMeasure() {
            return wateringMeasure;
        }

        public void setWateringMeasure(String wateringMeasure) {
            this.wateringMeasure = wateringMeasure;
        }

        public String getRoot() {
            return root;
        }

        public void setRoot(String root) {
            this.root = root;
        }

        @Override
        public String toString() {
            return "\t"+getRoot()+"{\n" +
                    "\t\ttempreture=" + tempreture + "\n" +
                    "\t\ttempretureMeasure=" + tempretureMeasure + "\n" +
                    "\t\tlightingLightRequiring=" + lightingLightRequiring + "\n" +
                    "\t\twatering=" + watering + "\n" +
                    "\t\twateringMeasure=" + wateringMeasure + "\n" +
                    "\t}\n";
        }
    }
    private String multiplying;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSoil() {
        return soil;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getMultiplying() {
        return multiplying;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public VisualParameters getVisualParameters() {
        return visualParameters;
    }

    public void setVisualParameters(VisualParameters visualParameters) {
        this.visualParameters = visualParameters;
    }

    public GrowingTips getGrowingTips() {
        return growingTips;
    }

    public void setGrowingTips(GrowingTips growingTips) {
        this.growingTips = growingTips;
    }

    @Override
    public String toString() {
        return getRoot()+"{\n" +
                "name=" + name + "\n" +
                "soil=" + soil + "\n" +
                "origin=" + origin + "\n" +
                "visualParameters=\n" + visualParameters +
                "growingTips=\n" + growingTips +
                "\nmultiplying=" + multiplying + "\n" +
                "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Flower flower = (Flower) o;
        return Objects.equals(root, flower.root) && Objects.equals(name, flower.name) && Objects.equals(soil, flower.soil) && Objects.equals(origin, flower.origin) && Objects.equals(visualParameters, flower.visualParameters) && Objects.equals(growingTips, flower.growingTips) && Objects.equals(multiplying, flower.multiplying);
    }

    @Override
    public int hashCode() {
        return Objects.hash(root, name, soil, origin, visualParameters, growingTips, multiplying);
    }

}

