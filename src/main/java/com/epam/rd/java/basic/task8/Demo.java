package com.epam.rd.java.basic.task8;

import com.epam.rd.java.basic.task8.controller.SAXController;
import com.epam.rd.java.basic.task8.models.Flower;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class Demo {
	
	public static void main(String[] args) throws Exception {
		Main.main(new String[] { "input.xml" });
	}
	
}